import { createRouter, createWebHistory } from 'vue-router';
// import Home from '../views/Home.vue';
import Guilds from '../views/Guilds.vue';

const routes = [
  {
    path: '/',
    name: 'Guilds',
    component: Guilds,
    children: [
      {
        path: '/guilds',
        name: 'Guilds',
        component: () =>
          import(/* webpackChunkName: "guilds" */ '../views/Guilds.vue')
      }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;

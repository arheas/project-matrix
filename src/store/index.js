import { createStore } from 'vuex';

export default createStore({
  state: {
    guildMembers: [
      {
        FirstName: 'Bogdan',
        LastName: 'I',
        guild: 'fe'
      },
      {
        FirstName: 'Bogdan',
        LastName: 'II',
        guild: 'be'
      }
    ]
  },
  mutations: {
    ADD_USER(state, member) {
      state.guildMembers.push(member);
    }
  },
  actions: {},
  getters: {
    guildMembers(state, guild) {
      return state.guildMembers.filter((item) => item.guild === guild);
    }
  },
  modules: {}
});
